import comms, load_cell, esp8266, config
from hardware import Scheduler, Mqtt, Wifi


scheduler = Scheduler()

wifi = Wifi(config.wifi_name,
            config.wifi_password)

mqtt = Mqtt(config.mqtt_server,
            config.mqtt_uid,
            config.mqtt_pwd,
            config.mqtt_topic,
            config.mqtt_keepalive)

c = comms.Comms(scheduler.delay, mqtt, wifi, mqtt.publish,
                config.heartbeat_interval)

r = load_cell.Sensor(esp8266.HX711, mqtt.publish)
mqtt.update_parameter = r.update_parameter


i = 0
while True:
    i += 1
    if i % 5 == 0:
        scheduler.tick()
        i = 0

    if r.update():
        r.on_event('steady')
    else:
        r.on_event('change')
