import time

import network
import ubinascii

from machine import unique_id
from umqtt_simple import MQTTClient


def client_id():
    return ubinascii.hexlify(unique_id()).decode('ascii')


class Scheduler:

    def __init__(self):
        self._once = {}

    def delay(self, interval, function, name, arg=None):
        if interval is None:
            print('scheduler: clear {}'.format(name))
            self._once.pop(id(function), None)
        else:
            print('scheduler: delay {} {} {}'.format(name, interval, arg))
            self._once[id(function)] = (time.time() + interval, function, name, arg)

    def tick(self):
        done = []

        for start_time, function, name, arg in self._once.values():
            if time.time() >= start_time:
                print('scheduler: run {} {}'.format(name, arg))
                function(arg)
                done.append(id(function))

        for function_id in done:
            self._once.pop(function_id, None)

    def loop(self):  # pragma: nocover
        while True:
            self.tick()
            time.sleep(1)


class Wifi:

    def __init__(self, wifi_name=None, wifi_password=None):
        self.wifi_name = wifi_name
        self.wifi_password = wifi_password

        # disable access point
        network.WLAN(network.AP_IF).active(False)

        self.wifi = network.WLAN(network.STA_IF)

    def isconnected(self):
        if self.wifi.isconnected():
            print('wifi: connected')
            return True
        else:
            print('wifi: not connected')
            return False

    def connect(self):
        if self.isconnected():
            return True

        print('wifi: connect...')
        self.wifi.active(True)
        self.wifi.connect(self.wifi_name, self.wifi_password)

        return self.isconnected()


class Mqtt:

    def __init__(self, mqtt_server, mqtt_uid, mqtt_pwd,
                 mqtt_topic, mqtt_keepalive):
        self.client_id = client_id()
        self.mqtt = MQTTClient(self.client_id,
                               mqtt_server,
                               keepalive=mqtt_keepalive,
                               user=mqtt_uid,
                               password=mqtt_pwd)
        self.mqtt.set_last_will(**self._status("OFFLINE"))
        self.mqtt.set_callback(self.message_callback)
        self.mqtt_topic = mqtt_topic
        self.update_parameter = None

        self._msg = None

    def _status(self, msg):
        return {'topic': 'status/' + self.client_id,
                'msg': msg, 'retain': True, 'qos': 1}

    def message_callback(self, topic, msg_raw):
        topic = topic.decode('ascii')
        print('mqtt: callback "{}"'.format(topic))
        if topic.startswith('update_config/'):
            print('mqtt: writing config.py')
            with open('config.py', 'w') as f:
                f.write(msg_raw)
        elif topic.startswith(self.mqtt_topic + '/config'):
            if callable(self.update_parameter):
                print('mqtt: {} {}'.format(topic, msg_raw))
                self.update_parameter(topic.split('/')[-1], msg_raw)

    def connect(self):
        try:
            print('mqtt: connect...')
            self.mqtt.connect()

            self.mqtt.subscribe('update_config/' + self.client_id)
            self.mqtt.subscribe(self.mqtt_topic + '/config/#')

            self.mqtt.publish(**self._status("ONLINE 1.0.0"))

        except Exception as err:
            print('mqtt: not connected')
            self.disconnect()
            return False

        else:
            print('mqtt: connected')
            return True

    def check(self):
        try:
            if self._msg:
                print('mqtt: sending queued msg')
                self.mqtt.publish(self.mqtt_topic, self._msg)
                self._msg = None

            self.mqtt.check_msg()
            self.mqtt.ping()

        except Exception as err:
            print('mqtt: error {}'.format(err))
            self.disconnect()
            return False

        else:
            print('mqtt: ok')
            return True

    def publish(self, msg, topic=''):
        print('mqtt: publish "{}"'.format(msg))

        try:
            self.mqtt.publish(self.mqtt_topic + topic, msg)
        except:
            print('mqtt: queued msg')
            self._msg = msg

    def disconnect(self):
        try:
            self.mqtt.disconnect()
        except:
            pass
