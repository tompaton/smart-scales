class State:

    transition = {}
    mute_events = []

    def __init__(self, notify, msg_format=None):
        self.notify = notify
        self.msg_format = (msg_format or '{{"old_state": "{old}", '
                           '"event": "{event}", "new_state": "{new}"}}')
        self.state = 'start'
        self.on_event('start')

    def on_event(self, event):
        old = self.state
        new = self.transition[old].get(event)

        if new:
            self.state = new
            getattr(self, new)()
        else:
            new = old

        if event not in self.mute_events:
            self.notify(self.msg_format.format(old=old, event=event, new=new))
