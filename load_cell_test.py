from unittest.mock import Mock, call

import load_cell


def test_start():
    hx711 = Mock()
    notify = Mock()
    r = load_cell.Sensor(hx711, notify,
                         msg_format="{old}:{event} -> {new}")
    assert r.state == 'unstable'
    assert hx711.mock_calls == []
    assert notify.mock_calls == [call('start:start -> unstable')]


def test_unstable_steady():
    hx711 = Mock()
    notify = Mock()
    r = load_cell.Sensor(hx711, notify,
                         msg_format="{old}:{event} -> {new}")
    r.measurement.append(1.2)
    assert r.state == 'unstable'
    r.on_event('steady')
    assert r.state == 'stable'
    assert hx711.mock_calls == []
    assert notify.mock_calls == [call('start:start -> unstable'),
                                 call('1.2', topic='/value'),
                                 call('1.2', topic='/raw')]


def test_stable_change():
    hx711 = Mock()
    notify = Mock()
    r = load_cell.Sensor(hx711, notify,
                         msg_format="{old}:{event} -> {new}")
    r.measurement.append(1.2)
    assert r.state == 'unstable'
    r.on_event('steady')
    assert r.state == 'stable'
    r.on_event('change')
    assert r.state == 'unstable'
    assert hx711.mock_calls == []
    assert notify.mock_calls == [call('start:start -> unstable'),
                                 call('1.2', topic='/value'),
                                 call('1.2', topic='/raw')]


# TODO: update, update_parameter
