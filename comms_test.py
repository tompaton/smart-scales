from unittest.mock import Mock, call

import comms


def test_start():
    timer = Mock()
    mqtt = Mock()
    wifi = Mock()
    wifi.connect.return_value = False
    notify = Mock()
    s = comms.Comms(timer, mqtt, wifi, notify,
                    msg_format="{old}:{event} -> {new}")
    assert s.state == 'offline'
    assert timer.mock_calls == [call(2, s.on_event, 'comms', 'heartbeat')]
    assert mqtt.mock_calls == []
    assert wifi.mock_calls == [call.connect()]
    assert notify.mock_calls == [call('start:start -> offline')]


def test_offline_heartbeat():
    timer = Mock()
    mqtt = Mock()
    mqtt.connect.side_effect = [False, True]
    wifi = Mock()
    wifi.connect.side_effect = [False, True, True]
    notify = Mock()
    s = comms.Comms(timer, mqtt, wifi, notify,
                    msg_format="{old}:{event} -> {new}")
    assert s.state == 'offline'
    s.on_event('heartbeat')
    s.on_event('heartbeat')
    assert timer.mock_calls == [call(2, s.on_event, 'comms', 'heartbeat'),
                                call(2, s.on_event, 'comms', 'heartbeat'),
                                call(0, s.on_event, 'comms', 'connected')]
    assert mqtt.mock_calls == [call.connect(), call.connect()]
    assert wifi.mock_calls == [call.connect(), call.connect(), call.connect()]
    assert notify.mock_calls == [call('start:start -> offline')]


def test_offline_connected():
    timer = Mock()
    mqtt = Mock()
    mqtt.connect.side_effect = [True]
    wifi = Mock()
    wifi.connect.side_effect = [True]
    notify = Mock()
    s = comms.Comms(timer, mqtt, wifi, notify,
                    msg_format="{old}:{event} -> {new}")
    assert s.state == 'offline'
    s.on_event('connected')
    assert timer.mock_calls == [call(0, s.on_event, 'comms', 'connected'),
                                call(30, s.on_event, 'comms','heartbeat')]
    assert mqtt.mock_calls == [call.connect(), call.check()]
    assert wifi.mock_calls == [call.connect(), call.isconnected()]
    assert notify.mock_calls == [call('start:start -> offline'),
                                 call('offline:connected -> online')]


def test_online_heartbeat():
    timer = Mock()
    mqtt = Mock()
    wifi = Mock()
    notify = Mock()
    s = comms.Comms(timer, mqtt, wifi, notify,
                    msg_format="{old}:{event} -> {new}")
    assert s.state == 'offline'
    s.on_event('connected')
    s.on_event('heartbeat')
    assert timer.mock_calls == [call(0, s.on_event, 'comms','connected'),
                                call(30, s.on_event, 'comms','heartbeat'),
                                call(30, s.on_event, 'comms','heartbeat')]
    assert mqtt.mock_calls == [call.connect(), call.check(), call.check()]
    assert wifi.mock_calls == [call.connect(), call.isconnected(), call.isconnected()]
    assert notify.mock_calls == [call('start:start -> offline'),
                                 call('offline:connected -> online')]


def test_online_disconnected_wifi():
    timer = Mock()
    mqtt = Mock()
    wifi = Mock()
    wifi.isconnected.return_value = False
    notify = Mock()
    s = comms.Comms(timer, mqtt, wifi, notify,
                    msg_format="{old}:{event} -> {new}")
    assert s.state == 'offline'
    s.on_event('connected')
    s.on_event('disconnected')
    assert timer.mock_calls == [call(0, s.on_event, 'comms','connected'),
                                call(0, s.on_event, 'comms','disconnected'),
                                call(0, s.on_event, 'comms','connected')]
    assert mqtt.mock_calls == [call.connect(), call.connect()]
    assert wifi.mock_calls == [call.connect(), call.isconnected(), call.connect()]
    assert notify.mock_calls == [call('start:start -> offline'),
                                 call('offline:connected -> online'),
                                 call('online:disconnected -> offline')]


def test_online_disconnected_mqtt():
    timer = Mock()
    mqtt = Mock()
    mqtt.check.return_value = False
    wifi = Mock()
    notify = Mock()
    s = comms.Comms(timer, mqtt, wifi, notify,
                    msg_format="{old}:{event} -> {new}")
    assert s.state == 'offline'
    s.on_event('connected')
    s.on_event('disconnected')
    assert timer.mock_calls == [call(0, s.on_event, 'comms','connected'),
                                call(0, s.on_event, 'comms','disconnected'),
                                call(0, s.on_event, 'comms','connected')]
    assert mqtt.mock_calls == [call.connect(), call.check(), call.connect()]
    assert wifi.mock_calls == [call.connect(), call.isconnected(), call.connect()]
    assert notify.mock_calls == [call('start:start -> offline'),
                                 call('offline:connected -> online'),
                                 call('online:disconnected -> offline')]
