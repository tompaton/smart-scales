from unittest.mock import Mock, call

import state


def test_start():

    class Machine(state.State):
        transition = {'start': {'start': 'A'},
                      'A': {}}

        ran = False

        def A(self):
            self.ran = True

    notify = Mock()
    s = Machine(notify)
    assert s.state == 'A'
    assert s.ran is True
    s.on_event('b')
    assert notify.mock_calls == [
        call('{"old_state": "start", "event": "start", "new_state": "A"}'),
        call('{"old_state": "A", "event": "b", "new_state": "A"}')]


def test_mute_events():

    class Machine(state.State):
        transition = {'start': {'start': 'A'},
                      'A': {}}
        mute_events = ['b']

        ran = False

        def A(self):
            self.ran = True

    notify = Mock()
    s = Machine(notify)
    assert s.state == 'A'
    assert s.ran is True
    s.on_event('b')
    assert notify.mock_calls == [
        call('{"old_state": "start", "event": "start", "new_state": "A"}')]
