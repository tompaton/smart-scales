from state import State


class Comms(State):

    transition = {
        'start': {
            'start': 'offline',
        },
        'offline': {
            'heartbeat': 'offline',
            'connected': 'online',
        },
        'online': {
            'heartbeat': 'online',
            'disconnected': 'offline',
        },
    }

    mute_events = ['heartbeat']

    def __init__(self, timer=None, mqtt=None, wifi=None, notify=None,
                 heartbeat_interval=30, msg_format=None):
        self.timer = timer
        self.mqtt = mqtt
        self.wifi = wifi
        self.heartbeat_interval = heartbeat_interval
        self.booted = False  # ugly hack for now

        super().__init__(notify, msg_format)

    def trigger(self, event, interval=0):
        if event == 'heartbeat':
            interval = self.heartbeat_interval if self.booted else 2
        self.timer(interval, self.on_event, 'comms', event)

    def offline(self):
        if not self.wifi.connect():
            return self.trigger('heartbeat')

        if not self.mqtt.connect():
            return self.trigger('heartbeat')

        self.booted = True

        return self.trigger('connected')

    def online(self):
        if not self.wifi.isconnected():
            return self.trigger('disconnected')

        if not self.mqtt.check():
            return self.trigger('disconnected')

        return self.trigger('heartbeat')
