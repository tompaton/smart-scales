import binascii
import sys
from unittest.mock import Mock, call

import pytest


@pytest.fixture
def hardware():
    for module in ['machine', 'micropython', 'network', 'time', 'umqtt_simple']:
        sys.modules[module] = Mock()

    sys.modules['ubinascii'] = binascii

    sys.modules['machine'].unique_id.return_value \
        = (0xDEADBEEF).to_bytes(4, 'big')

    if 'hardware' in sys.modules:
        del sys.modules['hardware']

    import hardware as hardware_module

    return hardware_module


def test_client_id(hardware):
    assert hardware.client_id() == 'deadbeef'


def test_scheduler(hardware):
    calls = []

    def func1(arg):
        calls.append(('func1', arg, hardware.time.time()))

    def func2(arg):
        calls.append(('func2', arg, hardware.time.time()))

    scheduler = hardware.Scheduler()

    for i in range(20):
        hardware.time.time.return_value = 1000 + i
        scheduler.tick()
        if i == 2:
            scheduler.delay(5, func1, 'func1', 'timer')  # 7
            scheduler.delay(3, func2, 'func2', 'timer')  # 5
        if i == 3:
            scheduler.delay(5, func2, 'func2', 'timer')  # rescheduled to 8
        if i == 9:
            scheduler.delay(2, func1, 'func1', 'two')  # 11
        if i == 10:
            scheduler.delay(None, func1, 'func1')  # cancel

    assert calls == [('func1', 'timer', 1007),
                     ('func2', 'timer', 1008)]
    assert scheduler._once == {}


def test_wifi_init(hardware):
    wifi = hardware.Wifi('ssid', 'pwd')

    assert hardware.network.mock_calls == [call.WLAN(hardware.network.AP_IF),
                                           call.WLAN().active(False),
                                           call.WLAN(hardware.network.STA_IF)]


def test_wifi_isconnected(hardware):
    wifi = hardware.Wifi('ssid', 'pwd')

    wifi.wifi.isconnected.return_value = True
    assert wifi.isconnected() is True

    wifi.wifi.isconnected.return_value = False
    assert wifi.isconnected() is False


def test_wifi_connect(hardware):
    wifi = hardware.Wifi('ssid', 'pwd')

    wifi.wifi.isconnected.side_effect = [False, True, True]
    assert wifi.connect() is True
    assert wifi.connect() is True

    assert wifi.wifi.mock_calls == [call.active(False),  # disable access point

                                    call.isconnected(),
                                    call.active(True),
                                    call.connect('ssid', 'pwd'),
                                    call.isconnected(),

                                    # second call just checks
                                    call.isconnected()]


def test_mqtt_init(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)

    assert hardware.MQTTClient.mock_calls == [
        call('deadbeef', 'host', user='uid', password='pwd', keepalive=60),
        call().set_last_will(topic='status/deadbeef', msg='OFFLINE',
                             qos=1, retain=True),
        call().set_callback(mqtt.message_callback),
    ]


def test_mqtt_connect(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)

    assert mqtt.connect() is True

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [
        call.connect(),
        call.subscribe('update_config/deadbeef'),
        call.subscribe('topic/config/#'),
        call.publish(topic='status/deadbeef', msg='ONLINE 1.0.0',
                     qos=1, retain=True),
    ]


def test_mqtt_connect_fail(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)

    mqtt.mqtt.connect.side_effect = Exception()

    assert mqtt.connect() is False

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [
        call.connect(),
        call.disconnect(),
    ]


def test_mqtt_check(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)

    assert mqtt.check() is True

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [call.check_msg(), call.ping()]


def test_mqtt_check_queued(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)
    mqtt._msg = 'queued'
    assert mqtt.check() is True
    assert mqtt._msg is None

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [
        call.publish('topic', 'queued'),
        call.check_msg(),
        call.ping(),
    ]


def test_mqtt_check_fail(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)
    mqtt.mqtt.check_msg.side_effect = Exception()
    mqtt.mqtt.disconnect.side_effect = Exception()

    assert mqtt.check() is False

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [
        call.check_msg(),
        call.disconnect(),
    ]


def test_mqtt_publish(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)

    mqtt.publish('msg')

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [call.publish('topic', 'msg')]


def test_mqtt_publish_topic(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)

    mqtt.publish('msg', topic='/sub')

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [call.publish('topic/sub', 'msg')]


def test_mqtt_publish_fail(hardware):
    mqtt = hardware.Mqtt('host', 'uid', 'pwd', 'topic', 60)
    mqtt.mqtt.publish.side_effect = Exception()

    mqtt.publish('msg')
    assert mqtt._msg == 'msg'

    # skip calls from init
    assert mqtt.mqtt.mock_calls[2:] == [call.publish('topic', 'msg')]
