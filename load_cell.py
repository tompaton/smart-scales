from math import sqrt
from state import State


class Sensor(State):

    transition = {
        'start': {
            'start': 'unstable',
        },
        'unstable': {
            'steady': 'stable',
        },
        'stable': {
            'change': 'unstable',
        },
    }
    mute_events = ['steady', 'change']

    def __init__(self, hx711=None, notify=None, msg_format=None):
        self.hx711 = hx711

        # parameters

        # exponential smoothing factor
        self.alpha = 0.1

        # steady when sigma less than threshold
        self.threshold = 0.005

        # scaling factor (m) & intercept (c) --> y = mx + c
        self.m = 1.0
        self.c = 0

        # raw values
        self.measurement = []

        # smoothed value for detecting stability
        self.mu = 0.0
        self.sigma = 10000.0

        super().__init__(notify, msg_format)

    def unstable(self):
        pass

    def stable(self):
        self.notify('{}'.format(self.value), topic='/value')
        self.notify('{}'.format(self.measurement[0]), topic='/raw')

    @property
    def value(self):
        x = sum(self.measurement) / len(self.measurement)
        return self.m * x + self.c

    def update(self):
        self.measurement.insert(0, self.hx711.read())
        del self.measurement[5:]

        error = self.measurement[0] - self.mu
        self.mu = self.mu + self.alpha * error
        self.sigma = sqrt((1.0 - self.alpha)
                          * (self.sigma * self.sigma
                             + self.alpha * error * error))

        # sudo cat /dev/ttyUSB0 \
        # | awk -Winteractive '/update/ {print $2 " " $3 - $4 " " $3 + $4}' \
        # | feedgnuplot --lines --stream --xlen 100
        print('update: {} {} {}'.format(self.measurement[0], self.mu, self.sigma))

        return abs(self.sigma / self.mu)  < self.threshold

    def update_parameter(self, topic, msg_raw):
        if topic in ('alpha', 'threshold', 'm', 'c'):
            print('{} {}'.format(topic, float(msg_raw)))
            setattr(self, topic, float(msg_raw))
