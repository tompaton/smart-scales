import sys
from unittest.mock import Mock, call

import pytest


@pytest.fixture
def esp8266():
    for module in ['hx711']:
        sys.modules[module] = Mock()

    import esp8266 as esp8266_module

    return esp8266_module


def test_esp8266(esp8266):
    assert esp8266.hx711.HX711.mock_calls == [call(d_out=4, pd_sck=5)]
