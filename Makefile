.PHONY: test pylint init update reset shell clean

# will re-use this Makefile inside the container
RUN = docker run -it --rm --device=/dev/ttyUSB0 -v `pwd`:/src registry.tompaton.com/tompaton/mpf make
RUN_LOCAL = docker run -it --rm -v `pwd`:/src registry.tompaton.com/tompaton/mpf make

test:
	$(RUN_LOCAL) test2
	python3 -m http.server --directory htmlcov/

pylint:
	$(RUN_LOCAL) pylint2

init:
	$(RUN) init2

update:
	$(RUN) update2

reset:
	$(RUN) reset2

shell:
	$(RUN) shell2

clean:
	rm *.mpy

# targets that run inside the container (cue Inception references...)
.PHONY: init2 update2 reset2 test2 shell2


# TODO: build these into a temporary /build directory within the container
LIBS = umqtt_simple.mpy hx711.mpy
CONFIG = config.py
MODS = comms.mpy hardware.mpy load_cell.mpy esp8266.mpy state.mpy


define MPF_RESET =
open ttyUSB0
rm main.py
endef
export MPF_RESET


define MPF_INIT =
open ttyUSB0
put umqtt_simple.mpy
put hx711.mpy
put config-production.py config.py
endef
export MPF_INIT


define MPF_UPDATE =
open ttyUSB0
put comms.mpy
put hardware.mpy
put load_cell.mpy
put esp8266.mpy
put state.mpy
put main.py
ls
endef
export MPF_UPDATE


shell2:
	mpfshell


reset2:
	echo "$$MPF_RESET" | mpfshell


init2: $(LIBS) $(CONFIG)
	echo "$$MPF_INIT" | mpfshell


update2: $(MODS)
	echo "$$MPF_UPDATE" | mpfshell


test2:
	python -m pytest -vv -ra . --cov=. --cov-report html --cov-branch


pylint2:
	python -m pylint *.py


%.mpy: %.py
	python -m mpy_cross $< -o $@
