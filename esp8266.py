import hx711

# D1 GPIO5 CLK
# D2 GPIO4 DAT
HX711 = hx711.HX711(d_out=4, pd_sck=5)
HX711.channel = HX711.CHANNEL_A_64
